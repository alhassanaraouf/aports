# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kosmindoormap
pkgver=20.12.0
pkgrel=1
# armhf blocked by qt5-qtdeclarative
# x86 fails in testsuite
arch="all !armhf !x86"
url="https://invent.kde.org/libraries/kosmindoormap"
pkgdesc="OSM multi-floor indoor map renderer"
license="BSD-2-Clause AND BSD-3-Clause MIT AND LGPL-2.0-or-later"
depends_dev="
	ki18n-dev
	kpublictransport-dev
	protobuf-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	bison
	extra-cmake-modules
	flex
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kosmindoormap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="2888d9e5f92275ffda5b01b12d74de8b67b53b04c24a5bf225f4e7bfc3be55b8931b1d4b3585c638d769e52823ce1d21f4a971b3770d0fc782c4b0d6a5533f89  kosmindoormap-20.12.0.tar.xz"
